<!DOCTYPE html>
<%@page import="homeautomation.Venue" %>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'device.label', default: 'Device')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#edit-device" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="edit-device" class="content scaffold-edit" role="main">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.device}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.device}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form resource="${this.device}" method="PUT">
                <g:hiddenField name="version" value="${this.device?.version}" />
                <fieldset class="form">
                    

                    <ul class="property-list">
                        <li class="fieldcontain">
                        <label>Name</label>
                        <span class="required-indicator">*</span>
                        <g:field type="text" name="deviceName" required="" value="${device?.name}"/>
                        </li>

                        <li class="fieldcontain">
                        <label >Address</label>
                        &nbsp;&nbsp;
                        <g:field type="text" name="deviceDescription" value="${device?.description}" />
                        </li>

                        <li class="fieldcontain">
                        <label >Mode(On/Off)</label>
                        <span class="required-indicator">*</span>
                        <g:checkBox name="deviceSwitch" checked="${device.mode}" style="clear:right; padding-right:260px;"/>

                        </li>

                        <li class="fieldcontain">
                        <label>Venue</label>
                        <span class="required-indicator">*</span>
                        <g:select optionKey="id" optionValue="name" name="venue" from="${Venue.findAll()}" />
                        </li>
                    </ul>



                </fieldset>
                <fieldset class="buttons">
                    <input class="save" type="submit" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
