<li class="fieldcontain">
    <span id="${label}" class="${label}"><g:message code="${label}" default="${label}" /> </span>
    <g:each in="${value}" var="v">
    <span class="property-value" aria-labelledby="${label}"><g:link controller="log" action="show" id="${v.id}">
    ${v.dateCreated}</g:link>
    </span>
    </g:each>
</li>