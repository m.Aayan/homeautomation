<li class="fieldcontain">
    <span id="${label}" class="${label}"><g:message code="${label}" default="${label}" /></span>
    <span class="property-value" aria-labelledby="${label}"><g:fieldValue bean="${device}" field="${property}"></g:fieldValue>
    </span>
</li>