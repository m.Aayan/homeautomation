<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'device.label', default: 'Device')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-device" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="show-device" class="content scaffold-show" role="main">
            <h1>device Details</h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <ul class="property-list">
                <li class="fieldcontain">
                    <span id="name" class="name" style="clear:right; padding-right:300px;" >
                    <g:message message="Name"/></span>
                    <span class="name-value" aria-labelledby="name"><g:fieldValue bean="${device}" field="name"></g:fieldValue>
                    </span>
                </li>

                <f:display bean="device" property="description"/>

                <li class="fieldcontain">
                    <span id="venue" class="venue" style="clear:right; padding-right:300px;" >
                    <g:message message="Venue"/></span>
                    <span class="venue-value" aria-labelledby="venue">
                    <g:link controller="venue" action="show" params="[venueId:device.venue.id]">
                        ${device.venue.name}</g:link>
                    </span>
                </li>

                <li class="fieldcontain">
                    <span id="mode" class="mode" style="clear:right; padding-right:300px;" >
                    <g:message message="Mode"/></span>
                    <span class="mode-value" aria-labelledby="mode">

                    <label style="font-weight:normal; text-align:left;">
                        <g:if test="${device.mode}">On</g:if>
                        <g:else>Off</g:else>
                    </label>

                    </span>
                </li>

                <f:display bean="device" property="logs"/>
            </ul>

            <g:form resource="${this.device}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit" action="edit" resource="${this.device}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
