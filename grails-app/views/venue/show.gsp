<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'venue.label', default: 'Venue')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-venue" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="show-venue" class="content scaffold-show" role="main">
            <h1>Venue Details</h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <ul class="property-list">
                <li class="fieldcontain">
                    <div>
                    <span id="name" class="name" style="clear:right; padding-right:300px;" >
                    <g:message message="Name"/></span>
                    <span class="name-value" aria-labelledby="name"><g:fieldValue bean="${venue}" field="name"></g:fieldValue>
                    </span>
                    </div>

                    <div>
                    <span id="address" class="address" style="clear:right; padding-right:287px;" >
                    <g:message message="Address"/></span>
                    <span class="address-value" aria-labelledby="address"><g:fieldValue bean="${venue}" field="address">
                    </g:fieldValue>
                    </span>
                    </div>

                    <div>
                    <f:display bean="venue" property="devices"/>
                    </div>

                </li>
            </ul>
            <g:form resource="${this.venue}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit" action="edit" resource="${this.venue}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
