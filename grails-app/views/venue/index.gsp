<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'venue.label', default: 'Venue')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#list-venue" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="list-venue" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <%--<f:table collection="${venueList}" />--%>

            <table>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Device</th>
                    </tr>
                </thead>
                <tbody>
                <g:each var="venue" in="${venueList}">
                    <tr>
                        <td>
                        <g:link controller="venue" action="show" params="[venueId:venue.id]">
                        ${venue.name}</g:link>
                        </td>
                        <td>${venue.address}</td>
                        <td>
                            <g:each var="device" in ="${venue.devices}">
                                <li>
                                <g:link controller="device" action="show" id="${device.id}">
                                ${device.name}</g:link>
                                </li>
                            </g:each>
                        </td>
                    </tr>
                </g:each>


                </tbody>
            </table>

            <div class="pagination">
                <g:paginate total="${venueCount ?: 0}" />
            </div>
        </div>
    </body>
</html>