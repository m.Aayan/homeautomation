<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'log.label', default: 'Log')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#list-log" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                
            </ul>
        </div>
        <div id="list-log" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>


            <table>
                <thead>
                    <tr>
                        <th>Device Name</th>
                        <th>Device Mode</th>
                        <th>Date Created</th>
                       
                    </tr>
                </thead>
                <tbody>
                <g:each var="log" in="${logList}">
                    <tr>
                        <td>
                        <g:link controller="device" action="show" id="${log.device.id}">
                        ${log.device.name}</g:link>
                        </td>
                        <td><g:if test="${log.deviceMode==false}">Off</g:if>
                        <g:else>On</g:else>
                        </td>
                        <td>${log.dateCreated}</td>
                        
                        
                    </tr>
                </g:each>


                </tbody>
            </table>

            <div class="pagination">
                <g:paginate total="${logCount ?: 0}" />
            </div>
        </div>
    </body>
</html>