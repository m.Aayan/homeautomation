<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'log.label', default: 'Log')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-log" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                
            </ul>
        </div>
        <div id="show-log" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>

            <ul class="property-list">
                <li class="fieldcontain">
                    <span id="dateCreated" class="dateCreated" style="clear:right; padding-right:260px;" >
                    <g:message message="Date Created"/></span>
                    <span class="dateCreated-value" aria-labelledby="dateCreated">${log.dateCreated}
                    </span>
                </li>

                <li class="fieldcontain">
                    <span id="deviceName" class="deviceName" style="clear:right; padding-right:260px;" >
                    <g:message message="Device Name"/></span>
                    <span class="deviceName-value" aria-labelledby="deviceName">${log.deviceName}
                    </span>
                </li>

                <li class="fieldcontain">
                    <span id="deviceMode" class="deviceMode" style="clear:right; padding-right:260px;" >
                    <g:message message="Device Mode"/></span>
                    <span class="deviceMode-value" aria-labelledby="deviceMode">
                        <g:if test="${log.device.mode}">On</g:if>
                        <g:else>Off</g:else>
                    </span>
                </li>

                <li class="fieldcontain">
                    <span id="device" class="device" style="clear:right; padding-right:300px;" >
                    <g:message message="Device"/></span>
                    <span class="device-value" aria-labelledby="device">
                    <g:link controller="device" action="show" id="${log.device.id}">
                    ${log.device.name}</g:link>
                    </span>

                </li>
            </ul>

            <g:form resource="${this.log}" method="DELETE">
                <fieldset class="buttons">
                    
                    <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
