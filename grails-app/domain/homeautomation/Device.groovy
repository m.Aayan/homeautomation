package homeautomation

class Device {
    String name
    String description
	boolean mode = false

    static hasMany=[logs:Log]
    static constraints={
        name(nullable:false)
        description(blank:true,nullable:true)
        mode(nullable:false)
    }
    static belongsTo=[venue:Venue]
}

