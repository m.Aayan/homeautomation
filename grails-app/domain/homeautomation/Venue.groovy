package homeautomation

class Venue {
    String name
    String address

    static hasMany=[devices: Device]
    static constraints = {
        name(nullable:false,blank:false)
        address(nullable:true,blank:false)
    }
}



