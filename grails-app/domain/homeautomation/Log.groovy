package homeautomation

class Log {
    Date dateCreated
    String deviceName
    boolean deviceMode

    static constraints={
        deviceName(nullable:false)
        deviceMode(nullable:false)
    }

    static belongsTo=[device:Device]
}
