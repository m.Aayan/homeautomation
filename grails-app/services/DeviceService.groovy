import homeautomation.Device
import homeautomation.Log
import homeautomation.Venue

class DeviceService {
    public def saveDevice(Device device,params){
        device.name=params.deviceName
        device.description=params.deviceDescription
        device.mode=false
        device.venue= Venue.get(params.venue)

        device.save flush:true

        Log log=new Log()
        log.device=device
        log.deviceName=device.name
        log.deviceMode=device.mode

        log.save flush:true
    }

    public def updateDevice(Device device,params){
        device.name=params.deviceName
        device.description=params.deviceDescription
        device.venue=Venue.get(params.venue)
        if(params?.deviceSwitch){
            device.mode=params.deviceSwitch
        }
        else{
            device.mode=params?._deviceSwitch
        }

        device.save flush:true

        Log log=new Log()
        log.device=device
        log.deviceName=device.name
        log.deviceMode=device.mode

        log.save flush:true
    }
}
