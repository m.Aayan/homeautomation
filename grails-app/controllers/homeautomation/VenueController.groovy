package homeautomation

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class VenueController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
//        respond Venue.list(params), model:[venueCount: Venue.count()]
        def venuelist = Venue.findAll()
        println("venuelist"+venuelist)
        render view:'index', model:[venueList: venuelist, venueCount: venuelist.size()]
    }

    def show() {
        Venue venue = Venue.get(params.venueId)
        respond venue
    }

    def create() {
        respond new Venue(params)
    }

    @Transactional
    def save(Venue venue) {
        if (venue == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (venue.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond venue.errors, view:'create'
            return
        }

        venue.save (flush:true)

        request.withFormat {
            form multipartForm {
                println("multipartform")
                flash.message = message(code: 'default.created.message', args: [message(code: 'venue.label', default: 'Venue'), venue.id])
                redirect(action: "show", params: [venueId: venue.id])
            }
            '*' {println("all others")
                respond venue, [status: CREATED] }
        }
    }

    def edit(Venue venue) {
        respond venue
    }

    @Transactional
    def update(Venue venue) {
        if (venue == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (venue.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond venue.errors, view:'edit'
            return
        }

        venue.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'venue.label', default: 'Venue'), venue.id])
                redirect(action: "show", params: [venueId: venue.id])
            }
            '*'{ respond venue, [status: OK] }
        }
    }

    @Transactional
    def delete(Venue venue) {

        if (venue == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        venue.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'venue.label', default: 'Venue'), venue.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'venue.label', default: 'Venue'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
