package homeautomation

import grails.test.mixin.TestFor
import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestFor(Device)
class DeviceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    @Unroll
    void "#name, #description, #mode is #result"(){
        expect:
        new Device(name: name, description: description, mode: mode).validate() == valid
        where:
        [name, description, mode]<<
                [['john Doe',' ',null],['NOW 1234',' ',null],['',false]].combinations()

        valid = name!=null && mode!=null && mode==(true||false)
        result = valid?'valid':'not valid'
    }
}