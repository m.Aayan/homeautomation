package homeautomation

import grails.test.mixin.TestFor
import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestFor(Venue)
class VenueSpec extends Specification {

    def cleanup() {
    }

    @Unroll
    void "#name, #address is #result"(){
        expect:
            new Venue(name: name, address: address).validate() == valid
        where:
            [name, address]<<
                    [['john Doe','',null],['nowWhere 1234','',null]].combinations()

        valid = name!=null && name!='' && address!=null && address!=''
        result = valid?'valid':'not valid'
    }

}