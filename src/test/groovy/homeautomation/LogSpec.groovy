package homeautomation

import grails.test.mixin.TestFor
import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestFor(Log)
class LogSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }
    @Unroll
    void "#deviceName, #deviceMode, #dateCreated is #result"(){
        expect:
        new Log(deviceName: deviceName, deviceMode: deviceMode, dateCreated: dateCreated).validate() == valid
        where:
        [deviceName, deviceMode, dateCreated]<<
                [['something','',null],[true,null],[new Date()]].combinations()

        valid = deviceName!=null && deviceName!='' && deviceMode!=null && dateCreated!=null
        result = valid?'valid':'not valid'
    }

}